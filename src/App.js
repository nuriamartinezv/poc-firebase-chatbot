import React, { useState, useRef, useEffect } from 'react';
import { GlButton, GL_BUTTON_VARIANT, GlInput, GlIcon, GlParagraph, GlHeading, GL_HEADING_AS } from '@adl/foundation';
// import sprite from '@adl/foundation/dist/adidas/assets/sprite.svg'
import './App.css';

// Firebase SDK
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

// Firebase Hooks
import { useAuthState } from 'react-firebase-hooks/auth'
import { useCollectionData } from 'react-firebase-hooks/firestore'


firebase.initializeApp({
  apiKey: "AIzaSyDbl4S76r0TEaHc5V8AIkLhFPhI3bsWbO4",
  authDomain: "poc-chatbot-b4305.firebaseapp.com",
  projectId: "poc-chatbot-b4305",
  storageBucket: "poc-chatbot-b4305.appspot.com",
  messagingSenderId: "347826953464",
  appId: "1:347826953464:web:80ffae85722215776a2f0d"
})

const auth = firebase.auth();
auth.setPersistence(firebase.auth.Auth.Persistence.NONE);

const firestore = firebase.firestore();

function App() {
  const [ user ] = useAuthState(auth)
  return (
    <div className="App">
      <header>
        <h2>Site</h2>
      </header>

      <section>
        {user ? <ChatRoom /> : <SignIn />}
      </section>

    </div>
  );
}

function SignIn() {
  const [token, setToken] = useState();

  useEffect(() => {
    fetch('http://localhost:5001/customToken', {
      method: "POST",
      credentials: 'include',
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(({token}) => {
        setToken(token)
        console.log(token)
      })
  }, []);

  const signInWithToken = () => {
    auth.signInWithCustomToken(token)
  }

  return (
    <GlButton className='chat-position' onClick={signInWithToken} icon=''>Start chat</GlButton>
  )
}

function ChatMessage(props) {
  const { text, uid } = props.message

  const isSentMessage = uid === auth.currentUser.uid
  const messageClass = isSentMessage ? 'gl-text-end' : 'gl-text-start';

  return (
    <div className='chat-message'>
      {!isSentMessage && <GlIcon name="logo"/>}
      <GlParagraph className={`${messageClass} col-s-12`}>{text}</GlParagraph>
    </div>
  )
}

function ChatRoom() {
  const { uid } = auth.currentUser;
  const aboveInputBoxArea = useRef()
  
  const messagesRef = firestore.collection(uid);
  const query = messagesRef.orderBy('createdAt').limit(25);

  const [messages] = useCollectionData(query);

  const [formValue, setFormValue] = useState('');
  const [isWriting, setIsWriting] = useState(false)

  const sendMessage = async(e) => {
    e.preventDefault();
    console.log(`user ${uid} has written`)

    await messagesRef.add({
      text: formValue,
      createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      uid
    })

    setFormValue('');
    aboveInputBoxArea.current.scrollIntoView({behavior: 'smooth'});
  }

  const onWriting = (event) => {
    setFormValue(event.target.value)
    setIsWriting(true)
  }

  return (
    <div className='chat-position chat-room'>
      <div className='messages-container'>
        <div>
          <button className='close-cta' onClick={() => auth.signOut()}>X</button>
          <GlHeading className='gl-vspace' as={GL_HEADING_AS.h4}>CHAT</GlHeading>
        </div>
        {messages && messages.map((msg, key) => <ChatMessage key={key} message={msg} />)}
        <div ref={aboveInputBoxArea}></div>
      </div>
      {isWriting && (
        <div className='is-writing gl-cta--loading'>
          <div className='gl-loader gl-cta__loading-spinner'></div>
          <GlParagraph>...is writting...</GlParagraph>
      </div>
      )}
      <div className='send-input-container gl-vspace'>
        <GlInput 
          onBlur={() => setIsWriting(false)}
          onChange={event =>  onWriting(event)}
          placeholder="Write here"
          value={formValue}
        />
        <GlButton onClick={sendMessage} variant={GL_BUTTON_VARIANT.secondary} icon='arrow-right-long'></GlButton>
      </div>
      
    </div>
  )
}

export default App;
