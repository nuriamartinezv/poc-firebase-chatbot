const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const cookieParser = require("cookie-parser");
const guid = require("./helpers");
const admin = require('firebase-admin');

const serviceAccount = require("../service-account.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const server = express();
const port = 5001;

server.use(bodyParser.json());
server.use(cors({credentials: true, origin: 'http://localhost:3000'}));
server.use(cookieParser());

server.post('/customToken', (req, res) => {
  const uid = guid()
  admin
    .auth()
    .createCustomToken(uid)
    .then((customToken) => {
      res.setHeader('Content-Type', 'application/json');
      res.end(JSON.stringify({ token: customToken }));
    })
    .catch((error) => {
      console.log('Error creating custom token:', error);
    });
})

server.listen(port, () => console.log(`EXPRESS SERVER:  http://localhost:${port}`));