const guid = () => {
  const _SLICES = [0, 2, 4, 8, 16];

  const chars = (slice) => {
    const char = `${Math.random().toString(_SLICES[4])}000000000`.substr(2, 8);

    return slice ? `-${char.substr(0, 4)}-${char.substr(4, 4)}` : char;
  };

  return chars() + chars(true) + chars(true) + chars();
};

module.exports = guid;